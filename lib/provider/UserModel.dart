import 'package:e_commerce_template/models/user.dart';
import 'package:flutter/foundation.dart';

class UserModel extends ChangeNotifier {
  final User _currentUser = User();

  User get currentUser => _currentUser;
}
