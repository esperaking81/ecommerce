import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'package:provider/provider.dart';
import '../../extensions/int_apis.dart';
import '../../extensions/widget_apis.dart';
import '../../extensions/string_apis.dart';
import '../../models/models.dart';
import '../../logic/logic.dart';

class TrackOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Order> orders = context.watch<OrderModel>()?.orders;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Track Order',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
      ),
      body: orders.isEmpty
          ? Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(FlutterIcons.history_faw5s, size: 60),
                16.vBox(),
                "You have not made any order yet !".widget()
              ],
            ).center()
          : ListView(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              children: orders.map((o) => _OrderElement(order: o)).toList(),
            ),
    );
  }
}

class _OrderElement extends StatelessWidget {
  const _OrderElement({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(order.id, style: TextStyle(fontSize: 10)),
                  const SizedBox(height: 8),
                  Text(
                    order.totalPrice.toString().dollars(),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  Spacer(),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2),
                      color: Colors.brown,
                    ),
                    child: Text(
                      order.status,
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            Spacer(),
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                image: DecorationImage(
                  image: NetworkImage(order.image),
                  fit: BoxFit.cover,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
