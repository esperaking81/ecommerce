import 'package:e_commerce_template/logic/logic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'checkout_item.dart';
import '../../extensions/product_apis.dart';
import '../../logic/cart_model.dart';

class CheckoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final products = context.watch<CartModel>().items;
    final List<Widget> checkOutWidgets =
        products.map((e) => CheckOutItem(e)).toList();
    final bool cartIsEmpty = context.watch<CartModel>().cartIsEmpty;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'My Cart',
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: cartIsEmpty
                  ? _EmptyState()
                  : ListView(children: checkOutWidgets),
            ),
            const SizedBox(height: 24),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: _TotalAndCheckoutAction(),
            )
          ],
        ),
      ),
    );
  }
}

class _EmptyState extends StatelessWidget {
  const _EmptyState({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AspectRatio(
          aspectRatio: 1 / 1,
          child: Image.asset('images/empty_cart.png'),
        ),
        const Text('Your cart is empty.')
      ],
    );
  }
}

class _TotalAndCheckoutAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bool cartIsEmpty = context.watch<CartModel>().cartIsEmpty;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('TOTAL'),
        const SizedBox(height: 4),
        Text(
          context.watch<CartModel>().items.cartTotal(),
          style: Theme.of(context).textTheme.headline6.copyWith(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w500),
        ),
        const SizedBox(height: 4),
        Text(
          'By clicking on checkout, you accept the terms and conditions of usage.',
        ),
        const SizedBox(height: 8),
        Container(
          height: 52,
          width: double.infinity,
          child: FlatButton(
            textColor: Colors.white,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              if (cartIsEmpty) {
                return null;
              }

              context.read<OrderModel>().newOrder();
              return Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(content: Text("Order created !")),
                );
            },
            child: Text(
              'checkout'.toUpperCase(),
            ),
          ),
        )
      ],
    );
  }
}
