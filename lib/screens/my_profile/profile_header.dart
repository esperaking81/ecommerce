import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'package:e_commerce_template/provider/UserModel.dart';

class ProfileHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      leading: Container(
        width: 60,
        height: 60,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: CachedNetworkImageProvider(
              context.watch<UserModel>().currentUser.profilePic,
            ),
            fit: BoxFit.cover,
          ),
        ),
      ),
      title: _UserDetails(),
      subtitle: Text(context.watch<UserModel>().currentUser.email),
    );
  }
}

class _UserDetails extends StatelessWidget {
  const _UserDetails({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          context.watch<UserModel>().currentUser.fullName,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
            color: Theme.of(context).primaryColor,
          ),
        ),
        const SizedBox(width: 8),
        if (context.watch<UserModel>().currentUser.online)
          Container(
            width: 8,
            height: 8,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
          )
      ],
    );
  }
}
