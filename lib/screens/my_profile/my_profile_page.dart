import 'package:e_commerce_template/screens/checkout/checkout_page.dart';
import 'package:e_commerce_template/screens/track_order/track_order_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../extensions/context_apis.dart';
import '../../screens/my_profile/profile_header.dart';

class MyProfilePage extends StatelessWidget {
  const MyProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.only(top: 24, left: 8, right: 8),
        children: [
          ProfileHeader(),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              Icons.edit_outlined,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Edit Profile',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              CupertinoIcons.location,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Shipping Address',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              CupertinoIcons.suit_heart,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'WishList',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              CupertinoIcons.time,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Order History',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            onTap: () {
              context.navigateTo(TrackOrderPage());
            },
            leading: Icon(
              CupertinoIcons.archivebox,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Track Order',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              CupertinoIcons.creditcard,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Cards',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              Icons.notifications_active_outlined,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Notifications',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
          const SizedBox(height: 16),
          ListTile(
            tileColor: Colors.grey.withOpacity(.05),
            leading: Icon(
              Icons.logout,
              color: Color.fromRGBO(103, 201, 135, 1),
            ),
            title: Text(
              'Sign Out',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios, size: 16),
          ),
        ],
      ),
    );
  }
}
