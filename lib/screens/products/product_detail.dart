import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../models/product.dart';
import '../../logic/cart_model.dart';

class ProductDetail extends StatelessWidget {
  final Product product;

  const ProductDetail(this.product, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        physics: NeverScrollableScrollPhysics(),
        slivers: [
          _MyImageAppBar(product: product),
          SliverFillRemaining(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _ProductDetails(product: product),
                    const SizedBox(height: 24),
                    _DescriptionDivider(),
                    const SizedBox(height: 24),
                    Text(product.description, textAlign: TextAlign.justify),
                    Spacer(),
                    _BuyNow(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _DescriptionDivider extends StatelessWidget {
  const _DescriptionDivider({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: MediaQuery.of(context).size.width * .6,
        child: Divider(height: 2, color: Colors.blueGrey),
      ),
    );
  }
}

class _BuyNow extends StatelessWidget {
  const _BuyNow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      width: MediaQuery.of(context).size.width,
      height: 52,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        gradient: LinearGradient(
          colors: <Color>[
            Color.fromRGBO(98, 130, 219, 1),
            Color.fromRGBO(75, 110, 206, 1),
          ],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      margin: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * .02,
      ),
      child: FlatButton(
        child: Text(
          'Buy Now',
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
        onPressed: () {},
      ),
    );
  }
}

class _MyImageAppBar extends StatelessWidget {
  const _MyImageAppBar({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    final bool inCart = context.watch<CartModel>().inCart(product);
    return SliverAppBar(
      expandedHeight: MediaQuery.of(context).size.height * .5,
      flexibleSpace: Hero(
        tag: product.image,
        child: Container(
          height: MediaQuery.of(context).size.height * .6,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(product.image),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      leading: GestureDetector(
        onTap: () => Navigator.pop(context),
        child: Container(
          margin: const EdgeInsets.only(left: 8),
          // padding: const EdgeInsets.only(left: 8),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.black38,
          ),
          child:
              Icon(FlutterIcons.long_arrow_alt_left_faw5s, color: Colors.white),
        ),
      ),
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 8),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.black38,
          ),
          child: IconButton(
            color: Colors.white,
            icon: Icon(
              inCart ? FlutterIcons.cart_minus_mco : FlutterIcons.cart_plus_mco,
            ),
            onPressed: () {
              if (inCart) {
                context.read<CartModel>().remove(product);
              } else {
                context.read<CartModel>().add(product);
              }
            },
          ),
        )
      ],
      bottom: PreferredSize(
        child: _FavoriteIcon(),
        preferredSize: Size(
          MediaQuery.of(context).size.width,
          MediaQuery.of(context).size.height * .1,
        ),
      ),
    );
  }
}

class _FavoriteIcon extends StatelessWidget {
  const _FavoriteIcon({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Spacer(),
        IconButton(
          icon: Icon(FlutterIcons.heart_faw),
          onPressed: () {},
        )
      ],
    );
  }
}

class _ProductDetails extends StatelessWidget {
  const _ProductDetails({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          product.wording,
          style: TextStyle(
            fontSize: 20,
            color: Theme.of(context).primaryColor,
          ),
          maxLines: 1,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
        ),
        const SizedBox(height: 4),
        Text(
          '\$${product.price}',
          style: TextStyle(
            fontSize: 14,
            color: Colors.blue,
            fontFamily: GoogleFonts.carroisGothic().fontFamily,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }
}
