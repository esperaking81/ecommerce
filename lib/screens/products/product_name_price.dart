import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../models/product.dart';

class ProductNameAndPrice extends StatelessWidget {
  final Product product;

  const ProductNameAndPrice(this.product, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          product.wording,
          style: TextStyle(fontSize: 12, color: Colors.white),
          maxLines: 1,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
        ),
        const SizedBox(height: 4),
        Text(
          '\$${product.price}',
          style: TextStyle(
            fontSize: 10,
            color: Colors.white,
            fontFamily: GoogleFonts.carroisGothic().fontFamily,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }
}
