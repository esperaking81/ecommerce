import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../logic/logic.dart';
import '../../extensions/context_apis.dart';
import 'product_detail.dart';
import 'product_widget.dart';

class ProductsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'All products',
        ),
      ),
      body: GridView.builder(
        padding: const EdgeInsets.all(16),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
        itemCount: context.watch<ProductModel>().products.length,
        itemBuilder: (ctx, i) {
          final product = ctx.watch<ProductModel>().products[i];
          return GestureDetector(
            onTap: () => ctx.navigateTo(ProductDetail(product)),
            child: ProuctItem(product: product),
          );
        },
      ),
    );
  }
}
