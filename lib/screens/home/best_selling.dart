import 'package:e_commerce_template/screens/products/product_detail.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../extensions/context_apis.dart';
import '../products/products_page.dart';
import '../../models/product.dart';
import '../../logic/logic.dart';
import 'styles.dart';

const sampleUrl =
    'https://ae01.alicdn.com/kf/H7fa979fb0b1e4b82ae6b9bbce9b7beaet/Men-Luxury-Watches-Quartz-Wrist-watch-Man-Sport-Analog-Wristwatch-Stainless-Steel-Casual-Bracele-Watch-Simple.jpg';

class BestSelling extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bestSellingProducts =
        context.watch<ProductModel>().products.getRange(0, 2);
    final bestSellings =
        bestSellingProducts.map((e) => _buildBestSeller(context, e)).toList();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Best Selling', style: getSectionTitleStyle(context)),
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => ProductsPage()),
                );
              },
              child: Text(
                'See All',
                style: getSubtitle2Style(context),
              ),
            ),
          ],
        ),
        const SizedBox(height: 8),
        GridView.count(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          crossAxisSpacing: 10,
          childAspectRatio: 1 / 2,
          crossAxisCount: 2,
          children: bestSellings,
        )
      ],
    );
  }

  Widget _buildBestSeller(BuildContext context, Product product) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: InkWell(
            onTap: () => context.navigateTo(ProductDetail(product)),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Image.network(product.image, fit: BoxFit.cover),
            ),
          ),
        ),
        const SizedBox(height: 8),
        Text(product.wording, style: getSectionTitleStyle(context)),
        const SizedBox(height: 4),
        Text(
          product.description,
          style: TextStyle(fontSize: 12),
        ),
        const SizedBox(height: 8),
        Builder(
          builder: (ctx) {
            final bool addedToCart =
                ctx.watch<CartModel>().items.contains(product);
            void addToCart() => context.read<CartModel>().add(product);

            return Container(
              width: double.infinity,
              child: FlatButton(
                onPressed: addedToCart ? null : addToCart,
                color: Theme.of(context).primaryColor,
                child: Text(addedToCart ? 'Added to Cart' : 'Add to Cart '),
                textColor: Colors.white,
              ),
            );
          },
        )
      ],
    );
  }
}
