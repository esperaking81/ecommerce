import 'package:e_commerce_template/screens/home/styles.dart';
import 'package:flutter/material.dart';

const List<Map<String, dynamic>> _categoryItems = [
  {'title': 'Men', 'icon': Icons.handyman},
  {'title': 'Woman', 'icon': Icons.pregnant_woman},
  {'title': 'Devices', 'icon': Icons.devices},
  {'title': 'Gadgets', 'icon': Icons.devices_other},
  {'title': 'Gaming', 'icon': Icons.games},
];

class Categories extends StatelessWidget {
  const Categories({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Categories', style: getSectionTitleStyle(context)),
        const SizedBox(height: 8),
        Container(
          height: 100,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _categoryItems.length,
            itemBuilder: (context, index) =>
                _buildCategoryItem(context, _categoryItems[index]),
          ),
        ),
      ],
    );
  }

  Widget _buildCategoryItem(
      BuildContext context, Map<String, dynamic> categoryItem) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).primaryColor,
              ),
              child: Icon(categoryItem['icon'], color: Colors.white),
            ),
          ),
          const SizedBox(height: 8),
          Text(
            categoryItem['title'] as String,
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
        ],
      ),
    );
  }
}
