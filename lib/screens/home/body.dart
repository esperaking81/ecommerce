import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../screens/home/best_selling.dart';
import '../../logic/logic.dart';
import 'categories.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isLoading = context.watch<ProductModel>().products.length == 0;

    return Container(
      width: double.infinity,
      padding: const EdgeInsets.only(left: 8, top: 16, right: 8),
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Categories(),
            const SizedBox(height: 8),
            if (isLoading)
              Container(
                height: 200,
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).primaryColor,
                      ),
                    )
                  ],
                ),
              )
            else
              BestSelling(),
          ],
        ),
      ),
    );
  }
}
