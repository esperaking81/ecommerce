import 'package:e_commerce_template/logic/logic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../home/bottom_app_bar.dart';
import '../my_cards/my_cards_page.dart';
import '../my_profile/my_profile_page.dart';
import '../offers/my_offers_page.dart';

import 'body.dart';
import 'header.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<Widget> screens = [
    _HomePage(),
    OffersPage(),
    MyCardsPage(),
    MyProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    int currentIndex = context.watch<HomeModel>().currentIndex;

    return Scaffold(
      backgroundColor:
          currentIndex == 0 ? Theme.of(context).primaryColor : null,
      body: SafeArea(child: screens[currentIndex]),
      bottomNavigationBar: MyBottomAppBar(
        currentIndex: currentIndex,
        onTap: (int newIndex) {
          context.read<HomeModel>().setIndex(newIndex);

          Brightness statusBrightness = Brightness.dark;

          if (newIndex == 3) {
            statusBrightness = Brightness.light;
          }

          SystemChrome.setSystemUIOverlayStyle(
            SystemUiOverlayStyle(statusBarBrightness: statusBrightness),
          );
        },
      ),
    );
  }
}

class _HomePage extends StatelessWidget {
  const _HomePage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [Header(), Expanded(child: Body())]);
  }
}
