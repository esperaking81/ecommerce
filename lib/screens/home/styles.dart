import 'package:flutter/material.dart';

TextStyle getSectionTitleStyle(BuildContext context) =>
    Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).primaryColor,
        fontSize: 16,
        fontWeight: FontWeight.w500);

TextStyle getSubtitle2Style(BuildContext context) => Theme.of(context)
    .textTheme
    .subtitle2
    .copyWith(color: Theme.of(context).primaryColor, fontSize: 14);
