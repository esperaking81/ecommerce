import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyBottomAppBar extends StatelessWidget {
  final Function(int newIndex) onTap;
  final int currentIndex;

  const MyBottomAppBar({Key key, this.onTap, this.currentIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Theme(
          data: Theme.of(context).copyWith(
            iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
          ),
          child: DefaultTextStyle(
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                InkWell(
                  onTap: () => onTap(0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (currentIndex == 0) _CurrentPageDot(),
                      Icon(Icons.home),
                      Text('Home'),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () => onTap(1),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (currentIndex == 1) _CurrentPageDot(),
                      Icon(Icons.article),
                      Text('My offers'),
                    ],
                  ),
                ),
                FloatingActionButton(
                  heroTag: 'main',
                  onPressed: null,
                  elevation: 4,
                  child: Icon(Icons.cloud_upload),
                  backgroundColor: Theme.of(context).primaryColor,
                ),
                InkWell(
                  onTap: () => onTap(2),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (currentIndex == 2) _CurrentPageDot(),
                      Icon(CupertinoIcons.creditcard),
                      Text('My Cards'),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () => onTap(3),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (currentIndex == 3) _CurrentPageDot(),
                      Icon(Icons.account_circle),
                      Text('My Profile'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _CurrentPageDot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 2),
      width: 40,
      height: 1,
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }
}
