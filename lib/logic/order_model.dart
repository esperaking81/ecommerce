import 'package:e_commerce_template/models/order.dart';
import 'package:flutter/foundation.dart';

class OrderModel extends ChangeNotifier {
  final List<Order> _orders = [];

  List<Order> get orders => List.unmodifiable(_orders);

  void newOrder() {
    _orders.add(
      Order(
        totalPrice: 4500,
        timestamp: DateTime.now().millisecondsSinceEpoch,
        image: 'https://images.unsplash.com/photo-1526170375885-4d8ecf77b99f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
      ),
    );
    notifyListeners();
  }
}
