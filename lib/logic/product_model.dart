import 'package:flutter/foundation.dart';
import 'package:supercharged/supercharged.dart';

import '../models/product.dart';

class ProductModel extends ChangeNotifier {
  final List<Product> _products = [];

  ProductModel() {
    3.seconds.delay.then((value) {
      _products.addAll(_demoProducts);
      notifyListeners();
    });
  }

  List<Product> get products => List.unmodifiable(_products);
}

final _demoProducts = <Product>[
  Product(
    wording: 'Cocooil',
    description: 'Organic Coconut Oil',
    image:
        'https://images.unsplash.com/photo-1526947425960-945c6e72858f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
    price: 120,
  ),
  Product(
    id: 'prod01',
    price: 200,
    wording: 'Tag Heuer Wristwatch',
    image:
        'https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
  ),
  Product(
    id: 'prod02',
    price: 150,
    wording: 'White Apple Watch',
    image:
        'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1889&q=80',
  ),
  Product(
    id: 'prod03',
    price: 150,
    wording: 'Vans',
    description: 'Vans Khalifa',
    image:
        'https://images.unsplash.com/photo-1525966222134-fcfa99b8ae77?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=943&q=80',
  ),
  Product(
    wording: 'Items Pack',
    description: 'Shoes, bag, watches...',
    image:
        'https://images.unsplash.com/photo-1511556820780-d912e42b4980?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80',
    price: 250,
  ),
  Product(
    wording: 'Polaroid',
    description: 'Best photo shooter',
    image:
        'https://images.unsplash.com/photo-1526170375885-4d8ecf77b99f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
    price: 220,
  ),
  Product(
    wording: 'Bike',
    description: 'Black bike',
    image:
        'https://images.unsplash.com/photo-1532298229144-0ec0c57515c7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1908&q=80',
    price: 300,
  ),
  Product(
    wording: 'Ray Ban Watches',
    description: 'Watches',
    image:
        'https://images.unsplash.com/photo-1567333126229-db29200c25f1?ixlib=rb-1.2.1&auto=format&fit=crop&w=975&q=80',
    price: 14,
  ),
  Product(
    wording: 'Nike shoes',
    description: 'Nikes pair',
    image:
        'https://images.unsplash.com/photo-1579828898622-446b9d65ff73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60',
    price: 130,
  ),
  Product(
    wording: 'Adapter',
    description: 'Headphones adapter',
    image:
        'https://images.unsplash.com/flagged/photo-1550713090-5a093719add2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60',
    price: 20,
  ),
  Product(
    wording: 'Hydrabio',
    description: 'Gel cream BIODERMA',
    image:
        'https://images.unsplash.com/photo-1597106776019-b4ecc878c202?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80',
    price: 6,
  ),
  Product(
    wording: 'Coke',
    description: 'Coca-cola Drink',
    image:
        'https://images.unsplash.com/photo-1591254467235-a82a70c915ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2089&q=80',
    price: 2,
  ),
  Product(
    wording: 'CHANEL',
    description: 'Chanel Paris',
    image:
        'https://images.unsplash.com/photo-1587304189936-28cedbc95827?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=975&q=80',
    price: 34,
  )
];
