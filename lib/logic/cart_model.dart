import 'package:flutter/foundation.dart';
import 'package:supercharged/supercharged.dart';

import '../models/product.dart';

class CartModel extends ChangeNotifier {
  /// Internal, private state of the cart.
  final List<Product> _items = [];

  /// An unmodifiable view of the items in the cart.
  List<Product> get items => List.unmodifiable(_items);

  /// The current total price of all items.
  int get totalPrice => _items.sumBy((i) => i.price);

  // currentState of cart
  bool get cartIsEmpty => _items.isEmpty;

  // product is already added to cart
  bool inCart(Product product) => _items.contains(product);

  /// Adds [Product] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  void add(Product item) {
    _items.add(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  void remove(Product item) {
    _items.remove(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  /// Removes all items from the cart.
  void removeAll() {
    _items.clear();
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
}
