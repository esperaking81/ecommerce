import 'package:flutter/material.dart';

extension Shopping on String {
  String dollars() {
    return '\$$this';
  }

  bool delivered() {
    return this == 'delivered';
  }

  Text widget({TextStyle style}) => Text(this, style: style);
}
