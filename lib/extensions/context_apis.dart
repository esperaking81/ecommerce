import 'package:flutter/material.dart';

extension Navigation on BuildContext {
  void navigateTo(Widget dest) {
    Navigator.of(this).push(MaterialPageRoute(builder: (_) => dest));
  }
}
