import 'package:flutter/material.dart';

extension SuperInt on int {
  SizedBox vBox() => SizedBox(height: this.toDouble());

  SizedBox hBox() => SizedBox(width: this.toDouble());
}
