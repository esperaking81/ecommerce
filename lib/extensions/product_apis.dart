import 'package:supercharged/supercharged.dart';

import '../models/product.dart';

extension CartTotal on List<Product> {
  String cartTotal() {
    return '\$${sumBy((p) => p.price)}';
  }
}
