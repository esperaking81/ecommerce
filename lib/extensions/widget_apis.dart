import 'package:flutter/material.dart';

extension SuperWidgets on Widget {
  Center center() => Center(child: this);
}
