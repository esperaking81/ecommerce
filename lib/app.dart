import 'package:e_commerce_template/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'logic/logic.dart';
import 'screens/home/home.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CartModel()),
        ChangeNotifierProvider(create: (_) => ProductModel()),
        ChangeNotifierProvider(create: (_) => OrderModel()),
        ChangeNotifierProvider(create: (_) => HomeModel()),
      ],
      child: MaterialApp(
        title: 'eCommerce',
        theme: ThemeData(
          // Define the default brightness and colors.
          primaryColor: primaryColor,
          accentColor: Colors.blue,
          appBarTheme: AppBarTheme(
            elevation: 0,
            color: Colors.transparent,
            titleTextStyle: TextStyle(
              fontFamily: GoogleFonts.expletusSans().fontFamily,
              color: primaryColor,
              fontSize: 18,
            ),
            iconTheme: IconThemeData(color: primaryColor),
          ),

          // Define the default font family.
          fontFamily: GoogleFonts.expletusSans().fontFamily,

          // Define the default TextTheme. Use this to specify the default

          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0),
            bodyText2: TextStyle(fontSize: 14.0),
          ),
        ),
        home: Home(),
      ),
    );
  }
}
