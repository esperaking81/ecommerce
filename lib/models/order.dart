import 'package:uuid/uuid.dart';

class Order {
  final String id;
  final int totalPrice;
  final int timestamp;
  final String status;
  final String image;

  Order({
    String id,
    this.totalPrice,
    this.status = 'In Transit',
    this.timestamp,
    this.image,
  }) : id = id ?? Uuid().v1().substring(0, 7);
}
