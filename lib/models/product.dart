import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

class Product {
  final String id;
  final String wording;
  final String description;
  final int price;
  final String image;

  Product({
    String id,
    @required this.wording,
    @required this.price,
    @required this.image,
    this.description = 'Lorem ipsum dolor sit amet',
  }) : id = id ?? Uuid().v4();
}
