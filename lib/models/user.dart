class User {
  static const String kProfilePic =
      'https://images.unsplash.com/photo-1511367461989-f85a21fda167?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60;';

  final String id;
  final String name;
  final String surname;
  final String email;
  final String profilePic;
  final bool online;

  User({
    this.id = 'user01',
    this.name = 'John',
    this.surname = 'DOE',
    this.email = 'john.doe@gmail.com',
    this.profilePic = kProfilePic,
    this.online = true,
  });

  String get fullName => '$name $surname';

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['name'],
      surname: json['surname'],
      email: json['email'],
      profilePic: json['profilePic'],
      online: json['online'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'surname': surname,
      'email': email,
      'profilePic': profilePic,
      'online': online,
    };
  }
}
